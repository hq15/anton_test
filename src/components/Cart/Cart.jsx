import React, {useContext} from 'react';
import Header from '../Header/Header';
import Button from '../Button/Button';
//import { ListItemContext } from '../ListItemContext/ListItemContext';
import Item from '../Item/Item';
import Modal from '../Modal/Modal';



const Cart = (props) => {

  const {
    cart,
    onDelete,
    clearCart,
    openModal,
    closeModal,
    show,
    modal,
    addToFavorite,
    favorite,
} = props
console.log(cart);

    return (
        <div> 
            <Header
                  itemCount={cart.length}
                  favCount={favorite.length}
                  actions = {
                    <>
                    <Button 
                    btnClassName="button"
                    className="button"
                    onClick={clearCart}
                    text="Очистить корзину"/>
                    </>
                  } />
                  <ul className="item-list">
           {cart.map(el => (
             < Item   
             classNameIcon={"favor-icon"}
             favorIgor={el.favorIgor}
             onClick={() => addToFavorite(el)}
             idForDelete={el.idForDelete}         
             modalId={el.id}
             key={el.idForDelete}
             imgRoute={el.imgRoute}
             name={el.name}
             price={el.price}   
             actions = {
               <>
               <Button 
               modalId={el.id}
               btnClassName="button apply"
               onClick={() => openModal(el.id)}
               className="button"
               text="Удалить из корзины"/>
               </>
             }
             />            
           ))}
         </ul>  
         {
         show && <Modal
         header="Подвердите удаление товара из корзины"
         onClick={closeModal}
         modalId={modal.id}  
           imgRoute={modal.imgRoute}
           name={modal.name}
           price={modal.price}
           actions = {
            <>
            <Button 
            modalId={modal.idForDelete} 
            btnClassName="button apply"
            onClick={() => onDelete(modal.idForDelete)}
            className="button"
            text="Удалить из корзины"
            />
            <Button 
            modalId={modal.idForDelete}
            btnClassName="button confirm"
            onClick={closeModal}
            className="button"
            text="Оформить заказ"
            />
            </>
          }
         />        
         }    
        </div>
    );
};

export default Cart;