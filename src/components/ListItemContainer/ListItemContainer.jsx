import React, {useState, useEffect } from 'react';
import { ListItemContext } from '../ListItemContext/ListItemContext';
import { useNavigate } from "react-router-dom";
import { logRoles } from '@testing-library/dom';

const ListItemContainer = (props) => {

    let [cards, setItems] = useState([]);
    let [cart, setCart] = useState([]);
    const [show, showModal] = useState(false);
    const [modal, modalForm] = useState();
    let [favorite, setFavorite] = useState([]); 
    let [favoritesId, setFavoritesId] = useState([])
  

    const openModal = (id) => {
      showModal(true);
      const dataForModal = cart.find(item => item.id === +id); 
      modalForm(dataForModal);  
    }

    const addToCart = (id) => {
        showModal(true);
        let dataForModal = cards.find(item => item.id === +id); 
        modalForm(dataForModal = {...dataForModal, idForDelete: Date.now()});
        cart.push(dataForModal);
        localStorage.setItem('cart', JSON.stringify(cart));
      }
    
        
    const closeModal = () => {
      showModal(false)  
    }
    

    const clearCart = () => {
        localStorage.removeItem('cart');
        setCart([]);
      }


      const handleDelete = (id) => {
        const newCart = cart.filter((el) => {
          if (el.idForDelete === id) {
            return false
          } else {
            return true
          }
        })
        setCart(newCart)
        localStorage.setItem('cart', JSON.stringify(newCart));
        closeModal();
      }
    
    useEffect(() => {
        fetch('./data/json-data.json')
        .then(response => response.json())
        .then(item => {
          setItems(item);
        //if (localStorage.getItem("cart") !== null) {setCart(JSON.parse(localStorage["cart"]))};
        })
    }, []);



  //     useEffect(() => {
  //       if (localStorage.getItem("favorites") !== null) {setFavorite(JSON.parse(localStorage["favorites"]))
  // };
  // }, [])
  
  



  
 

  favoritesId = ((JSON.parse(localStorage["favorites"]))).map((item) => item.id);
  //setFavoritesId(favoritesId)
  console.log(favoritesId);
  let cartId = ((JSON.parse(localStorage["cart"]))).map((item) => item.id);
  

    let cardsWithfavorIgor = cards.map((el) => {
      (favoritesId.includes(el.id))
        ? (el.favorIgor = true)
        : (el.favorIgor = false);
        
      return el;
      
    });

    const filterBy = (a, b) => {
      let typedArr = a.filter(function (a) {
        return a.id === b.find((item) => item === a.id);
      });
      return typedArr;
    };
    const cartProducts = filterBy(cards, cartId);
    const favoritesProducts = filterBy(cards, favoritesId);
    console.log(favoritesProducts);
    //setFavorite(favoritesProducts)

  
    // useState([cardsWithfavorIgor])
    //console.log(cards);
    
// let favoritesForRend = cards.map((el) => {
//   if (el.favorIgor === true) {
//     return el
//   }
  
  
// })
    
//console.log(favoritesForRend);



       const addToFavorite = (el) => {
        console.log(favorite.includes(el));
        console.log(favorite);
        console.log(el);
        if (favorite.includes(el)) {
          
          el.favorIgor = false
            const newFavorites = favorite.filter((item) => item.id !== el.id);
            
            console.log(newFavorites);
            setFavorite(newFavorites);
            //setFavoritesId(favoritesId)

        } else {
          el.favorIgor = true
            setFavorite(arr => [...arr, el])
          
            //setFavoritesId(favoritesId)

        };
        
    }

  //   useEffect(() => {
  //     localStorage.setItem("favorites", JSON.stringify(favorite));
  // }, [favorite])


  const { children } = props;

console.log(cards);
    return (
        <ListItemContext.Provider value={{
            cards,
            cart: cart,
            //setCart: setCart,
            clearCart: clearCart,
            onDelete: handleDelete,
            addToCart: addToCart,
            closeModal: closeModal,
            addToFavorite: addToFavorite,
            openModal: openModal,
            show,
            modal,
            favorite,
        }}>
        {children}
        </ListItemContext.Provider>
      );
    
};

export default ListItemContainer;