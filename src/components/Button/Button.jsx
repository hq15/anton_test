import React from "react";
import "./Button.scss";
import PropTypes from 'prop-types';



const Button = (props) => {
  return (
<>
    <button className={props.btnClassName}
      data-id={props.modalId}
      onClick={props.onClick}
    >
    {props.text}
    </button>
    </>
  );
};

export default Button;


Button.propTypes = {
  modalId: PropTypes.number,
  onClick: PropTypes.func,
  btnClassName: PropTypes.string,
};


