import React, {useContext, useEffect, useState } from 'react';
import Header from '../Header/Header';
import Button from '../Button/Button';
import { ListItemContext } from '../ListItemContext/ListItemContext';
import Item from '../Item/Item';
import Modal from '../Modal/Modal';
import { useNavigate } from "react-router-dom";



const Favorites = (props) => {

  const navigate = useNavigate();
  
  const {
    cards,
    cart,
    clearCart,
    closeModal,
    addToCart,
    show,
    modal,
    addToFavorite,
    favorite,
} = props

const moveTo = (e) => {
  const route = "/cart";
   navigate(route)
   closeModal()
}

console.log(favorite);
    return (
        <div> 
            <Header
                  itemCount={cart.length}
                  favCount={favorite.length}
                  actions = {
                    <>
                    <Button 
                    btnClassName="button"
                    className="button"
                    onClick={clearCart}
                    text="Очистить корзину"/>
                    </>
                  } />
                  <ul className="item-list">
           {favorite.map(el => (
            //(el.favorIgor === true) && (
             < Item         
             modalId={el.id}
             classNameIcon={"favor-icon"}
             favorIgor={el.favorIgor}
             onClick={() => addToFavorite(el)}
             id={el.id}
             key={el.id}
             imgRoute={el.imgRoute}
             name={el.name}
             price={el.price}   
             actions = {
              <>
              <Button 
              modalId={el.id}
              btnClassName="button apply"
              onClick={() => addToCart(el.id)}
              className="button"
              text="Добавить в корзину"/>
              </>
           
            }
             /> 
          //  )           
           ))}
         </ul>  
         {
         show && <Modal
         header="Подвердите удаление товара из корзины"
         onClick={closeModal}
         modalId={modal.id}  
           imgRoute={modal.imgRoute}
           name={modal.name}
           price={modal.price}
           actions = {
            <>
            <Button 
            btnClassName="button apply"
            onClick={moveTo}
            className="button"
            text="Перейти в корзину"
            />
            <Button 
            btnClassName="button confirm"
            onClick={closeModal}
            className="button"
            text="Продолжить покупки"
            />
            </>
          }
         />        
         }    
        </div>
    );
};

export default Favorites;