import './Reset.css';
import './App.scss'

import React, {useState, useEffect } from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ItemList from './components/ItemList/ItemList';
import Cart from './components/Cart/Cart';
import ListItemContainer from './components/ListItemContainer/ListItemContainer';
import Favorites from './components/Favorites/Favorites';



const App = () => {

  const [cards, setItems] = useState([]);
  const [cart, setCart] = useState([]);
  const [show, showModal] = useState(false);
  const [modal, modalForm] = useState();
  const [favorite, setFavorite] = useState([]);  


  const openModal = (id) => {
    showModal(true);
    const dataForModal = cart.find(item => item.id === +id); 
    modalForm(dataForModal);  
  }

  const addToCart = (id) => {
      showModal(true);
      let dataForModal = cards.find(item => item.id === +id); 
      modalForm(dataForModal = {...dataForModal, idForDelete: Date.now()});
      cart.push(dataForModal);
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  
      
  const closeModal = () => {
    showModal(false)  
  }
  

  const clearCart = () => {
      localStorage.removeItem('cart');
      setCart([]);
    }


    const handleDelete = (id) => {
      const newCart = cart.filter((el) => {
        if (el.idForDelete === id) {
          return false
        } else {
          return true
        }
      })
      setCart(newCart)
      localStorage.setItem('cart', JSON.stringify(newCart));
      closeModal();
    }
  
  useEffect(() => {
      fetch('./data/json-data.json')
      .then(response => response.json())
      .then(item => {
        setItems(item);
      if (localStorage.getItem("cart") !== null) {setCart(JSON.parse(localStorage["cart"]))};
      })
  }, []);



    useEffect(() => {
      if (localStorage.getItem("favorites") !== null) {setFavorite(JSON.parse(localStorage["favorites"]))
};
}, [])


useEffect(() => {
 const newCards = cards.map((el) => {
    favorite.includes(el)
      ? (el.favorIgor = true)
      : (el.favorIgor = false);
    return el;
  });
  setItems(newCards)
}, [favorite, cart])

     const addToFavorite = (el) => {
      if (favorite.includes(el)) {
          const newFavorites = favorite.filter((item) => item.id !== el.id);
          setFavorite(newFavorites);
      } else {
          setFavorite(arr => [...arr, el])
      };
  }

  useEffect(() => {
    localStorage.setItem("favorites", JSON.stringify(favorite));
}, [favorite])

  


const filterBy = (a, b) => {
  let typedArr = a.filter(function (a) {
    return a.id === b.find((item) => item === a.id);
  });
  return typedArr;
};

const favoritesId = ((JSON.parse(localStorage["favorites"]))).map((item) => item.id);
console.log(favoritesId);
let cardsWithfavorIgor = cards.map((el) => {
  (favoritesId.includes(el.id))
    ? (el.favorIgor = true)
    : (el.favorIgor = false);
    
  return el;
  
});

      
  const favoritesProducts = filterBy(cardsWithfavorIgor, favoritesId);
  
  

  let cartId = ((JSON.parse(localStorage["cart"]))).map((item) => item.id);

  const cartProducts = filterBy(cards, cartId);
  



  return (
    <BrowserRouter>
    
      <Routes>
        <Route path="/" element={
        <ItemList
        cards={cards}
        cart={cart}
        clearCart={clearCart}
        addToCart={addToCart}
        closeModal={closeModal}
        addToFavorite={addToFavorite}
        show={show}
        modal={modal}
        favorite={favorite}
         />
        } /> 
        <Route path="favorites" element={
        <Favorites
          cards={cards}
          cart={cart}
          clearCart={clearCart}
          closeModal={closeModal}
          addToCart={addToCart}
          show={show}
          modal={modal}
          addToFavorite={addToFavorite}
          favorite={favoritesProducts}
        />
        } /> 
        <Route path="cart" element={
        <Cart
          cart={cartProducts}
          onDelete={handleDelete}
          clearCart={clearCart}
          openModal={openModal}
          closeModal={closeModal}
          show={show}
          modal={modal}
          addToFavorite={addToFavorite}
          favorite={favorite}
        />
        } /> 
      </Routes>
      
    </BrowserRouter>
  );
};

export default App;

